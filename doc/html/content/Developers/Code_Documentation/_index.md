---
Title: "Code interna"
Weight: 2
Description: "How to contribute to the code."
---

In this part of the web, some more details on the implementation are discussed.

{{% children depth=1 %}}
