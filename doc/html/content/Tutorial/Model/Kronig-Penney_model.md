---
title: "Kronig-Penney Model"
#tags: ["Beginner", "Ground State", "Model", "Chain", "Band Structure", "User-defined Species", "Independent Particles"]
section: "/home/lueders/Downloads/Tutorial Kronig-Penney_Model"
series: "Tutorials"
tutorials: "Model Systems"
theories: "Independent particles"
calculation_modes: "Ground state"
system_types: "Chain"
species_types: "User-defined species"
features: "Band structure"
difficulties: "beginner"
difficulties_weight: 2
description: "Calculate the bandstructure for Kronig-Penney Model."
---


The Kronig-Penney model is a 1D system that demonstrates band gaps, which relate to the allowed energies for electrons in a material. In this tutorial we calculate the bandstructure for Kronig-Penney Model. The Kronig-Penney Model has a periodic potential of

$$
V(x) =
\begin{cases}
      V_0 & -b < x < 0 \cr
      0 & 0 < x < a
\end{cases}
$$

Where b is the width of each barrier, and a is the spacing between them. 

## Input
The following input file will be used for the ground state calculation:

{{< code-block >}}
#include_input doc/tutorials/model_systems/kronig_penney_model/1.gs/inp
{{< /code-block >}}

{{< figure src="/images/Kp_wavefunctions.png" width="500px" caption="The first two wavefunctions plotted alongside the potential." >}}


## Bandstructure
To calculate the bandstructure simply change the {{< variable "CalculationMode" >}} to unocc.

{{< code-block >}}
#include_input doc/tutorials/model_systems/kronig_penney_model/2.unocc/inp
{{< /code-block >}}


#include_eps doc/tutorials/model_systems/kronig_penney_model/2.unocc/bandstructure.eps caption="The band structure for Kronig-Penney Model."


To plot the bandstructure, we will use the same command from the {{< tutorial "Periodic_Systems" "Periodic Systems" >}} (assuming you are using gnuplot).

{{< code-block >}}
 plot for [col=5:5+9] 'static/bandstructure' u 1:(column(col)) w l notitle ls 1
{{< /code-block >}}

Reference:
Sidebottom DL. Fundamentals of condensed matter and crystalline physics: an introduction for students of physics and materials science. New York: Cambridge University Press; 2012.



{{< tutorial-footer >}}





