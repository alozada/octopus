---
Title: "Gaussian-shaped external current density"
series: "Tutorials"
tutorials: "Maxwell"
Weight: 14
---

### Gaussian-shaped external current density passed by a Gaussian temporal current pulse

#### No absorbing boundaries

{{< expand "click for complete input" >}}
{{< code-block >}}
#include_input doc/tutorials/maxwell/3.external-current/1.gaussian_current_pulse/inp
{{< /code-block >}}
{{< /expand >}}

Instead of an incoming external plane wave, Octopus can simulate also external current
densities placed inside the simulation box. In this example we place one shape of such
a current density in the simulation box

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/3.external-current/1.gaussian_current_pulse/inp boundaries
{{< /code-block >}}

Since we start with no absorbing boundaries, we reset the box size to 10.0.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/3.external-current/1.gaussian_current_pulse/inp box
{{< /code-block >}}

The external current density is switched on by the corresponding options and
two blocks define its spatial distribution and its temporal behavior. The
spatial distribution of our example external current is a Gaussian distribution
in 3D. The temporal pulse is one Gaussian along the y-axis and one along the
opposite direction but time shifted.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/3.external-current/1.gaussian_current_pulse/inp current
{{< /code-block >}}

{{% expand "gnuplot script" %}}
```
set pm3d
set view map
set palette defined (-0.005 "blue", 0 "white", 0.005"red")
set term png size 1000,500

unset surface
unset key

set output 'plot1.png'

set xlabel 'x-direction'
set ylabel 'y-direction'
set cbrange [-0.005:0.005]

set multiplot

set origin 0.025,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.252788 au)'
sp [-10:10][-10:10][-0.01:0.01] 'Maxwell/output_iter/td.0000120/e_field-z.z=0' u 1:2:3

set origin 0.525,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.379182 au)'
sp [-10:10][-10:10][-0.01:0.01] 'Maxwell/output_iter/td.0000180/e_field-z.z=0' u 1:2:3

unset multiplot
```
{{% /expand %}}

Contour plot of the electric field in z-direction after 120 time steps for
t=0.24 and 180 time steps for t=0.36:
{{< figure src="/images/Maxwell/tutorial_04.1-plot1.png" width="50%" >}}



#### Mask absorbing boundaries

{{< expand "click for complete input" >}}
{{< code-block >}}
#include_input doc/tutorials/maxwell/3.external-current/2.gaussian_current_pulse_with_mask/inp
{{< /code-block >}}
{{< /expand >}}

We can add now mask absorbing boundaries.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/3.external-current/2.gaussian_current_pulse_with_mask/inp boundaries
{{< /code-block >}}

Accordingly to the additional absorbing width, we have to update the simulation
box dimensions.
{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/3.external-current/2.gaussian_current_pulse_with_mask/inp box
{{< /code-block >}}


Contour plot of the electric field in z-direction after 120 time steps for
t=0.24 and 180 time steps for t=0.36:
{{< figure src="/images/Maxwell/tutorial_04.2-plot1.png" width="50%" >}}

#### PML boundaries

{{< expand "click for complete input" >}}
{{< code-block >}}
#include_input doc/tutorials/maxwell/3.external-current/3.gaussian_current_pulse_with_pml/inp
{{< /code-block >}}
{{< /expand >}}


We can repeat the simulation using PML absorbing boundaries.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/3.external-current/3.gaussian_current_pulse_with_pml/inp boundaries
{{< /code-block >}}

Contour plot of the electric field in z-direction after 120 time steps for
t=0.24 and 180 time steps for t=0.36:
{{< figure src="/images/Maxwell/tutorial_04.3-plot1.png" width="50%" >}}


Maxwell fields at the origin and Maxwell energy inside the free Maxwell
propagation region of the simulation box:
{{< figure src="/images/Maxwell/tutorial_04_maxwell_energy_and_fields.png" width="50%" >}}

<!---
Contour plot of the electric field in z-direction after 120 time steps for t=0.24 and 180 time steps for t=0.36:
{{< figure src="/images/Maxwell/tutorial_08_run_electric_field_contour.png" width="720px" >}}
--->

{{< tutorial-footer >}}
