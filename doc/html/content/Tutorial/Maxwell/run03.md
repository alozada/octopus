---
Title: "Cosinoidal plane wave hitting a linear medium box"
series: "Tutorials"
tutorials: "Maxwell"
Weight: 13
---

## Cosinoidal plane wave hitting a linear medium box

An arbitrary number of linear medium shapes can be placed inside the Maxwell
simulation box.

Linear media are considered a separate system type, for example:

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/2.linear-medium/1.cosinoidal_pulse_td/inp systems
{{< /code-block >}}


The box shape can be defined in two ways, which are given by the variable
{{<variable "LinearMediumBoxShape">}}: if set to medium_parallelepiped, the
parallelepiped box will be defined by its center, and size in each dimension
through the {{<variable "LinearMediumBoxSize">}} block; if the box shape is
defined as medium_box_file, the box shape will be read from an external OFF
file, defined through the variable {{<variable "LinearMediumBoxFile">}}. To
produce such files, check the {{<tutorial "Maxwell/details/creating_geometries" 
"Creating geometries" >}} tutorial. In either case, the
electromagnetic properties of each medium must be defined in the {{<variable
"LinearMediumProperties">}} block, which specifies the relative electric
permittivity, relative magnetic permeability, and the electric and magnetic
conductivities (for lossy media). Finally, the spatial profile assumed to
calculate gradients at the box edges is defined through the {{<variable
"LinearMediumEdgeProfile">}} variable, and can be either "edged" or "smooth"
(for linear media shape read from file, it can only be edged).

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/2.linear-medium/1.cosinoidal_pulse_td/inp medium_box
{{< /code-block >}}

Note that we also need to change the variables describing the propagation.
{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/2.linear-medium/1.cosinoidal_pulse_td/inp timestep
{{< /code-block >}}

{{< notice note >}}
Linear media are static. Therefore only the propagator
{{<code-inline>}}{{<variable "TDSystemPropagator">}} = static
{{</code-inline>}} is permitted.  By moving the variable {{<variable
"TDSystemPropagator">}} into the {{< code "Maxwell" >}} namespace, the default
propagator (which is {{<code "static">}}) is automatically inherited by the
{{<code "Medium">}} system and only the propagator for the {{<code
"Maxwell">}} system is explicitly set to {{<code "exp_mid">}}. As the {{<code
"exp_mid">}} propagator contains two algorithmic steps, it must be clocked
twice as fast as the {{<code "static">}} propagator and hence the time step 
of the medium bust be set to half the Maxwell time step. This is currently a
workaround, and is likely to change in future releases of the code.
{{< /notice >}}

### No absorbing boundaries

{{< expand "click for complete input" >}}
{{< code-block >}}
#include_input doc/tutorials/maxwell/2.linear-medium/1.cosinoidal_pulse_td/inp
{{< /code-block >}}
{{< /expand >}}


To illustrate a laser pulse hitting a linear medium box, in addition to
adding the linear_medium system, we need to switch the Maxwell Hamiltonian
operator from the 3x3 vacuum representation to the 6x6 coupled representation
with the six component Riemann-Silberstein vector that includes also the complex
conjugate Riemann-Silberstein vector.


{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/2.linear-medium/1.cosinoidal_pulse_td/inp calculation
{{< /code-block >}}

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/2.linear-medium/1.cosinoidal_pulse_td/inp box
{{< /code-block >}}

For this run, we use the previous incident plane wave propagating only in the
x-direction but place a medium box inside the Simulation box.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/2.linear-medium/1.cosinoidal_pulse_td/inp field
{{< /code-block >}}

{{% expand "gnuplot script" %}}
```
set pm3d
set view map
set palette defined (-0.05 "blue", 0 "white", 0.05"red")
set term png size 1000,500

unset surface
unset key

set output 'plot1.png'

set xlabel 'x-direction'
set ylabel 'y-direction'
set cbrange [-0.05:0.05]

set multiplot

set origin 0.025,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.105328 au)'
sp [-10:10][-10:10] 'Maxwell/output_iter/td.0000050/e_field-z.z=0' u 1:2:3

set origin 0.525,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.210656 au)'
sp [-10:10][-10:10] 'Maxwell/output_iter/td.0000100/e_field-z.z=0' u 1:2:3

unset multiplot

set output 'plot2.png'

set multiplot

set origin 0.025,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.263321 au)'
sp [-10:10][-10:10] 'Maxwell/output_iter/td.0000125/e_field-z.z=0' u 1:2:3

set origin 0.525,0
set size 0.45,0.9
set size square
set title 'Electric field E_z (t=0.315985 au)'
sp [-10:10][-10:10] 'Maxwell/output_iter/td.0000150/e_field-z.z=0' u 1:2:3

unset multiplot

```
{{% /expand %}}

Contour plot of the electric field in z-direction after 50 time steps for
t=0.11 and 100 time steps for t=0.21:
{{< figure src="/images/Maxwell/tutorial_03.1-plot1.png" width="50%" >}}

Contour plot of the electric field in z-direction after 125 time steps for
t=0.26 and 150 time steps for t=0.32:
{{< figure src="/images/Maxwell/tutorial_03.1-plot2.png" width="50%" >}}

In the last panel, it can be seen that there is a significant amount of
scattered waves which, in large parts, are scattered from the box boundaries.
In the following we will use different boundary conditions, in order to reduce
this spurious scattering.


### Mask absorbing boundaries

{{< expand "click for complete input" >}}
{{< code-block >}}
#include_input doc/tutorials/maxwell/2.linear-medium/2.cosinoidal_pulse_td_mask/inp
{{< /code-block >}}
{{< /expand >}}

The previous run was without absorbing boundaries, now we switch on the mask
function that damps the field at the boundary by multiplying a scalar mask
function. The {{<variable "MaxwellAbsorbingBoundaries">}} block is updated to
run with mask absorbing. The {{<variable "MaxwellABMaskWidth">}} is set to
five.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/2.linear-medium/2.cosinoidal_pulse_td_mask/inp boundaries
{{< /code-block >}}

As a consequence of the additional region for the absorbing boundary condition,
we have to change the box size to still obtain the same free Maxwell
propagation box. Therefore, the {{<code "lsize_mx">}} value is now 17.0, which
is the previously used size that includes the incident wave boundary width plus
the absorbing boundary width. For more details, see the information on 
{{<tutorial "Maxwell/details/simulationbox" "simulation boxes">}}

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/2.linear-medium/2.cosinoidal_pulse_td_mask/inp box
{{< /code-block >}}

Contour plot of the electric field in z-direction after 50 time steps for
t=0.11 and 100 time steps for t=0.21:
{{< figure src="/images/Maxwell/tutorial_03.2-plot1.png" width="50%" >}}

Contour plot of the electric field in z-direction after 125 time steps for
t=0.26 and 150 time steps for t=0.32:
{{< figure src="/images/Maxwell/tutorial_03.2-plot2.png" width="50%" >}}

It can be seen that the scattering is slightly reduced, but still noticeable.

### Perfectly matched layer boundaries

{{< expand "click for complete input" >}}
{{< code-block >}}
#include_input doc/tutorials/maxwell/2.linear-medium/3.cosinoidal_pulse_td_pml/inp
{{< /code-block >}}
{{< /expand >}}


The mask absorbing method can be replaced by the more accurate perfectly
matched layer (PML) method. Therefore, the {{<variable
"MaxwellAbsorbingBoundaries">}} by the {{<code "cpml">}} option. The PML
requires some additional parameters to the width for a full definition.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/2.linear-medium/3.cosinoidal_pulse_td_pml/inp boundaries
{{< /code-block >}}


Contour plot of the electric field in z-direction after 50 time steps for
t=0.11 and 100 time steps for t=0.21:
{{< figure src="/images/Maxwell/tutorial_03.3-plot1.png" width="50%" >}}

Contour plot of the electric field in z-direction after 125 time steps for
t=0.26 and 150 time steps for t=0.32:
{{< figure src="/images/Maxwell/tutorial_03.3-plot2.png" width="50%" >}}

The PML boundary conditions in this case are comparable to the mask boundary conditions,
but lead to an increased computational cost.

{{< tutorial-footer >}}
