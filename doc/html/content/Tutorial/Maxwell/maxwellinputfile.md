---
Title: "Maxwell input file"
series: "Tutorials"
tutorials: "Maxwell"
weight: 4
---

### Maxwell Input File

#### Input file variable description

##### Calculation mode and parallelization strategy


At the beginning of the input file, the basic option variable {{< variable
"CalculationMode" >}} selects the run mode of Octopus and has always to be set.
In case of a parallel run, there are some variables to set the proper
parallelization options. For Maxwell propagation, parallelization in domains is
possible, but parallelization in states is not needed, as there are always 3 or
6 states, depending on the Hamiltonian (see below).

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/00-maxwell-input-options.inp calc_mode
{{< /code-block >}}


##### Multisystem setup

The Maxwell system is now implemented in Octopus' multisystem framework, which
allows to calculate several different systems, which can interact with each
other.

Currently implemented system types are:

* electronic: An electronic system. (only partly implemented)
* maxwell: A maxwell system.
* classical_particle: A classical particle. Used for testing purposes only.
* charged_particle: A charged classical particle.
* multisystem: A system containing other systems.
* dftbplus: A DFTB+ system
* linear_medium: A linear medium for classical electrodynamics.


##### Definition of the systems

In this tutorial, we will use a pure Maxwell system:

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/00-maxwell-input-options.inp system
{{< /code-block >}}

Subsequently, variables relating to a named system are prefixed by the system
name. However, this prefix is not mandatory for calculations considering only
one system.

##### Maxwell box variables and parameters

The Maxwell box is only defined as a parallelepiped box.

{{< expand "Example" >}}

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/00-maxwell-input-options.inp box
{{< /code-block >}}
{{< /expand >}}


##### Maxwell options

The Maxwell options determine the Maxwell propagation scheme. First, the
Maxwell propagation operator options consist of the type of {{< variable
"MaxwellHamiltonianOperator" >}}, which can be set as a vacuum
Maxwell-Hamiltonian (faraday_ampere) or a Maxwell-Hamiltonian in linear medium
(faraday_ampere_medium).

Currently, the Maxwell system does not define its own propagator, but uses the
system default propagator, defined in {{< variable "TDSystemPropagator" >}}. So
far, only the exponential midpoint is implemented.

The Maxwell Boundary conditions can be chosen for each direction differently
with {{< variable "MaxwellBoundaryConditions" >}}. Possible choices are zero and
absorbing boundary conditions, as well as plane waves and constant field
boundary conditions. Additionally to the boundary condition, the code can
include absorbing boundaries. This means that all outgoing waves are absorbed
while all incoming signals still arise at the boundaries and propagate into
the simulation box. The absorbing boundaries can be achieved by a mask
function or by a perfectly matched layer (PML) calculation with additional
parameters. For more information on the physical meaning of these parameters,
please refer to the [Maxwell-TDDFT paper].

[Maxwell-TDDFT paper]: https://doi.org/10.1080/00018732.2019.1695875

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/00-maxwell-input-options.inp maxwell_calc
{{< /code-block >}}

##### Output options

The output option {{< variable "OutputFormat" >}} can be used exactly as in the
case of TDDFT calculations, with the exception that some formats are inconsistent
with a Maxwell calculation (like xyz). It is possible to chose multiple
formats. The Maxwell output options are defined in the {{< variable
"MaxwellOutput" >}} block (written to output_iter every {{< variable
"MaxwellOutputInterval" >}} steps) and through the {{< variable
"MaxwellTDOutput" >}} (written to td.general every step).

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/00-maxwell-input-options.inp output2
{{< /code-block >}}


##### Time step variables

The {{< variable "TDTimeStep" >}} option in general defines the time step used
for each system. For the stability of the time propagation, it is recommended for
Maxwell systems to always fulfill the Courant condition. In some cases larger
time steps are possible but this should be checked case by case.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/00-maxwell-input-options.inp timestep
{{< /code-block >}}


##### Maxwell field variables

The incident plane waves can be defined at the boundaries using the {{<
variable "MaxwellIncidentWaves" >}} block. This block defines the incoming wave
type and complex electric field amplitude, and a Maxwell envelope function
name, which must be defined in a separate block. Multiple plane waves can be
defined, and their superposition gives the final incident wave result at the
boundary. This plane waves boundaries are evaluated at each timestep, and give
the boundary conditions for the propagation inside the box. To use them {{<
variable "MaxwellBoundaryConditions" >}} must be set to "plane_waves" in the
necessary directions.

The {{< variable "MaxwellFunctions" >}} block reads the additional required
information for the plane wave pulse, for each envelope function name given in
the {{< variable "MaxwellIncidentWaves" >}} block.

Inside the simulation box, the initial electromagnetic field is set up by the
{{< variable "UserDefinedInitialMaxwellStates" >}} block. If the pulse
parameters are such that it is completely outside the box at the initial time,
this variables does not need to be set, as the default initial field inside the
box is zero.

In case of {{< code-inline >}}{{< variable "MaxwellPlaneWavesInBox" >}} = yes
{{< /code-inline >}}, the code uses the initial analytical plane wave values
also inside the simulation box. Hence, there is no Maxwell field propagation
and the values are set analytically.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/00-maxwell-input-options.inp field
{{< /code-block >}}

##### External current

An external current can be switched that adds an external current contribution
to the internal current. Such an external current is calculated analytically by
the {{< variable "UserDefinedMaxwellExternalCurrent" >}} block. This block
defines the spatial profile and frequency of each external current
contribution, and it sets an envelope function name, that must be defined in a
separate {{< variable "TDFunctions" >}} block.

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/00-maxwell-input-options.inp ext_current
{{< /code-block >}}


##### Linear Medium

Different shapes of linear media can be included in the calculation either
through a simple box, or through a file describing more complex geometries. For
more information check the Linear Medium tutorial.


{{< tutorial-footer >}}

