---
Title: "Interference of two cosinoidal plane waves"
series: "Tutorials"
tutorials: "Maxwell"
Weight: 12
---

## Interference of two cosinoidal plane waves


Instead of only one plane wave, we simulate two different plane waves with
different wave-vectors entering the simulation box, interfering and leaving the
box again. In addition to the wave from the last tutorial, we add a second
wave with different wave length, and entering the box at an angle, and shifted
by 28 Bohr along the corresponding direction of propagation.

{{< expand "click for complete input" >}}
{{< code-block >}}
#include_input doc/tutorials/maxwell/1.free-propagation/2.2_pulses_td/inp
{{< /code-block >}}
{{< /expand >}}

Both electric fields are polarized only in z-direction, and the magnetic field
only in y-direction.

We can start from the last input file, and add the second wave, according to
the following excerpt:

{{< code-block >}}
#include_input_snippet doc/tutorials/maxwell/1.free-propagation/2.2_pulses_td/inp field
{{< /code-block >}}

Contour plot of the electric field in z-direction after 50 time steps for
t=0.11 and 100 time steps for t=0.21:
{{< figure src="/images/Maxwell/tutorial_02_run_electric_field_contour.png" width="50%" >}}

Maxwell fields at the origin and Maxwell energy inside the free Maxwell
propagation region of the simulation box:
{{< figure src="/images/Maxwell/tutorial_02_run_maxwell_energy_and_fields.png" width="50%" >}}

{{< tutorial-footer >}}
