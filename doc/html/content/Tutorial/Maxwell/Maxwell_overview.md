---
Title: "Maxwell overview"
series: "Tutorials"
tutorials: "Maxwell"
weight: 1
---


## Tutorial for Maxwell propagation and its coupling with TDDFT in Octopus

#### Authors: Rene Jestädt, Franco Bonafé and Heiko Appel

#### Max Planck Institute for the Structure and Dynamics of Matter - Theory Department


This tutorial is based on the following publication:

[Light-matter interactions within the Ehrenfest–Maxwell–Pauli–Kohn–Sham
framework: fundamentals, implementation, and nano-optical
applications](./Light_matter_interactions_within_the_Ehrenfest_Maxwell_Pauli_Kohn_Sham_framework_fundamentals_implementation_and_nano_optical_applications.pdf)
René Jestädt, Michael Ruggenthaler, Micael J. T. Oliveira, Angel Rubio, and
Heiko Appel https://doi.org/10.1080/00018732.2019.1695875

---

[Slides of Heiko Appel's talk](https://theory.mpsd.mpg.de/talks/heiko.appel/2020-01-21-Uni-Jena)

---

Using the Riemann-Silberstein representation for the Maxwell's equations, the
corresponding time-evolution of Maxwell fields is implemented as quantum
mechanical like propagation in the TDDFT code Octopus.

The program can be run in a free Maxwell propagation mode where the
electromagnetic fields are calculated inside a simulation box. This box can
include various shapes for linear media. An initial electromagnetic field can be
set up in the box as well as an external current density or incoming plane
waves at the box boundaries. The simulation box boundaries can be selected as
zero boundaries, perfectly electric conductor (PEC mirror), perfectly magnetic
conductor (PMC) or absorbing boundaries with different methods.

Combining all features of a free Maxwell propagation with the normal TDDFT
matter propagation in Octopus, the code can solve fully coupled
Maxwell-Kohn-Sham systems by propagating both systems on two separated grids.
In this case, the Kohn-Sham system gives rise to an internal current density that
influences the Maxwell field propagation, and in turn the electromagnetic field
is part of the Kohn-Sham Hamiltonian. Both coupled systems are propagated
self-consistently, but the code can also be used for only forward or only
backward coupling.

In the following we introduce the different input options for the free Maxwell
propagation and the fully coupled Maxwell-Kohn-Sham propagation, and different
examples to show the various features.

<!-- All the input files can be downloaded [here](./maxwelltddft_tutorial_files.tar). -->

---

#### Simulation box and relevant input file variables

* {{< tutorial "maxwell/simulationbox" "Simulation box" >}}
* {{< tutorial "maxwell/maxwellinputfile" "Input file variables" >}}




#### Examples of electromagnetic field propagation without coupling to TDDFT

* {{< tutorial "maxwell/run01" "Cosinoidal plane wave in vacuum" >}}
* {{< tutorial "maxwell/run02" "Interference of two cosinoidal plane waves" >}}
* {{< tutorial "maxwell/run03" "Cosinoidal plane wave hitting a linear medium box with and without absorbing boundaries" >}}
* {{< tutorial "maxwell/run04" "Gaussian-shaped spatial external current distribution density passed by a Gaussian temporal current pulse" >}}


<!--
#### Coupled Maxwell-TDDFT propagation

* {{< tutorial "maxwell/benzene" "Benzene ground state and dynamics coupled to an external EM pulse" >}}


To check what will be the input syntax once the Maxwell propagation branch is merged into the current version of Octopus and how to obtain and compile the code, click on {{< tutorial "multisystem" "Multisystems" >}}

-->
{{< tutorial-footer >}}

