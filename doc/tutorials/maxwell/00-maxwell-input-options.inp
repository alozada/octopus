
# snippet_start calc_mode
# Calculation mode of the octopus run
CalculationMode = td
# snippet_end

# snippet_start system
ExperimentalFeatures = yes
%Systems
  'Maxwell' | maxwell
%
# snippet_end

# snippet_start box
# ----- Maxwell box variables ----------------------------------------------
    
# Variable of the Maxwell grid size
lsize_mx = 12.0
    
# Variable of the Maxwell grid spacing
dx_mx = 0.5
    
# Number of dimensions of the Maxwell grid
Maxwell.Dimensions = 3
    
# Box shape of the Maxwell simulation box
Maxwell.BoxShape = parallelepiped
    
# Box size of the Maxwell simulation box in each dimension
    
%Maxwell.Lsize
lsize_mx | lsize_mx | lsize_mx
%
    
# Grid spacing of the Maxwell simulation box in each dimension
    
%Maxwell.Spacing
dx_mx | dx_mx | dx_mx
%
#snippet_end

# snippet_start maxwell_calc
# ----- Maxwell calculation variables -----------------------

# Type of Maxwell Hamiltonian operator

# - faraday_ampere (3x3 for vacuum calculation)
# - faraday_ampere_medium (6x6 for medium calculation)

MaxwellHamiltonianOperator = faraday_ampere

# Options for the Maxwell derivative stencil and the exponential expanison of the time propagator

# - Maxwell.DerivativesStencil = stencil_star
# - Maxwell.DerivativesStencil = stencil_starplus
# - Maxwell.DerivativesStencil = stencil_cube

Maxwell.DerivativesStencil = stencil_starplus
Maxwell.DerivativesOrder = 4
Maxwell.TDExpOrder = 4


# Maxwell boundary conditions for each dimension
# - MaxwellBoundaryConditions = zero        (Boundaries are set to zero.)
# - MaxwellBoundaryConditions = constant    (Boundaries are set to a constant.)
# - MaxwellBoundaryConditions = mirror_pec  (Perfect electric conductor.)
# - MaxwellBoundaryConditions = mirror_pmc  (Perfect magnetic conductor.)
# - MaxwellBoundaryConditions = plane_waves (Boundaries feed in plane waves.)

%MaxwellBoundaryConditions
plane_waves | plane_waves | plane_waves
%


# Maxwell absorbing boundaries options
# - MaxwellAbsorbingBoundaries = not_absorbing (zero boundary condition)
# - MaxwellAbsorbingBoundaries = mask (Maxwell field is muliplied by a mask function)
# - MaxwellAbsorbingBoundaries = cpml (Maxwell PML with convolution method)

%MaxwellAbsorbingBoundaries
not_absorbing | not_absorbing | not_absorbing
%

# Absorbing boundary width for the Maxwell mask function (if using masked boundaries)
MaxwellABWidth = 5.0

# Parameters to tune the Maxwell PML (they have safe defaults)
# - numerical tests show best performance for MaxwellABPMLPower between 2 and 3
# - MaxwellABPMLReflectionError

MaxwellABPMLPower = 2.0
MaxwellABPMLReflectionError = 1e-16
# snippet_end


# snippet_start output2
# ----- Maxwell output variables ----------------------------------------------------------------

# Maxwell output variables are written every MaxwellOutputInterval time steps into the output_iter folder
# %MaxwellOutput
#   electric_field (electric field output)
#   magnetic_field (magnetic field output)
#   trans_electric_field (transverse electric field output)
#   trans_magnetic_field (transverse magnetic field output)
#   long_electric_field (longitudinal electric field output)
#   long_magnetic_field (longitudinal magnetic field output)
#   div_electric_field (divergence of the electric field output)
#   div_magnetic_field (divergence of the magnetic field output)
#   maxwell_vector_potential (vector potential output)
#   poynting_vector (poynting vector output)
#   maxwell_energy_density (electromagnetic energy density output)
#   maxwell_current (electromagnetic current density on the Maxwell grid)
#   external_current (external current density on the Maxwell grid)
#   electric_dipole_potential (electric dipole potential output)
#   electric_quadrupole_potential (electric quadrupole potential output)
#   charge_density (electromagnetic charge density via div(E))
# %

%MaxwellOutput
  electric_field
  magnetic_field
%

# Output interval steps for the Maxwell variables. After MaxwellOutputInterval steps,
# the Maxwell variables on the grid are written into the output_iter folder
MaxwellOutputInterval = 1

# Output of the scalar Maxwell variables for each time step, written into the td.general folder

# - MaxwellTDOutput = maxwell_energy (electromagnetic and Maxwell-matter energies)
# - MaxwellTDOutput = maxwell_total_e_field (electric field at all points given by MaxwellFieldsCoordinate)
# - MaxwellTDOutput = maxwell_total_b_field (magnetic field at all points given by MaxwellFieldsCoordinate)
# - MaxwellTDOutput = maxwell_longitudinal_e_field (longitudinal electric at all points given by MaxwellFieldsCoordinate)
# - MaxwellTDOutput = maxwell_longitudinal_b_field (longitudinal magnetic field at all points given by MaxwellFieldsCoordinate)
# - MaxwellTDOutput = maxwell_transverse_e_field (transverse field at all points given by MaxwellFieldsCoordinate)
# - MaxwellTDOutput = maxwell_transverse_b_field (transverse field at all points given by MaxwellFieldsCoordinate)
# - MaxwellTDOutput = maxwell_mean_poynting (mean poynting vector in each direction)
# - MaxwellTDOutput = maxwell_poynting_surface (mean poynting vector of boundary surfaces)

MaxwellTDOutput = maxwell_energy + maxwell_total_e_field

# Coordinates of the grid points, which corresponding electromagnetic field values are
# written into td.general/maxwell_fields
%MaxwellFieldsCoordinate
0.00 | 0.00 | 0.00
1.00 | 0.00 | 1.00
%
# snippet_end

# snippet_start timestep
# ----- Time step variables ---------------------------------------------------------------------

TDSystemPropagator = exp_mid

# Time step of the propagation
# TDTimeStep should be equal or smaller than the Courant criterion, which is here
# S_Courant = 1 / (sqrt(c^2/dx_mx^2 + c^2/dx_mx^2 + c^2/dx_mx^2))
TDTimeStep = 0.002

# Total simulation time
TDPropagationTime = 0.4
# snippet_end

# snippet_start field
# ----- Maxwell field variables -----------------------------------------------------------------

# Plane waves input block
# - Column 1: input method (in this example, a a predefined Maxwell function)
# - Column 2: electric field complex amplitude in the x- direction
# - Column 3: electric field complex amplitude in the y- direction
# - Column 4: electric field complex amplitude in the z- direction
# - Column 5: Maxwell function name
%MaxwellIncidentWaves
plane_wave_mx_function | Ex1 | Ey1 | Ez1 | "plane_waves_func_1"
plane_wave_mx_function | Ex2 | Ey2 | Ez2 | "plane_waves_func_2"
%

# Predefined Maxwell function
# - Column 1: Maxwell function name that corresponds to MaxwellIncidentWaves
# - Column 2: envelope function of the plane wave pulse
# - Column 3: wavevector component in x-direction
# - Column 4: wavevector component in y-direction
# - Column 5: wavevector component in z-direction
# - Column 6: pulse shift in x-direction
# - Column 7: pulse shift in y-direction
# - Column 8: pulse shift in z-direction
# - Column 9: pulse width
# kxl, kx2, ky1, ky1, ky2, kz1, kz2 = wavevectors in x,y,z-direction for EM fields 1 and 2
# psx1, psx2, psy1, psy2, psz1, psz2 = spatial shift of pulse 1 and 2 in x,y,z-direction
# pw1, pw2 = pulse width of pulse 1 and 2
%MaxwellFunctions
"plane_waves_func_1" | mxf_cosinoidal_wave | kx1 | ky1 | kz1 | psx1 | psy1 | psz1 | pw1
"plane_waves_func_2" | mxf_cosinoidal_wave | kx2 | ky2 | kz2 | psx2 | psy2 | psz2 | pw2
%

# Definition of the initial EM field inside the box:
# - Column 1: input type (formula, file or "use_incident_waves")
# If input type is formula or file:
#   - Column 2: field component
#   - Column 3: what kind of Maxwell field
#   - Column 4: analytic formula or file input for the field component
%UserDefinedInitialMaxwellStates
  use_incident_waves
%

# If incident plane waves should be used to calculate the analytical Maxwell field inside the simulation box (no propagation, only evaluation of the plane waves)
MaxwellPlaneWavesInBox = no
# snippet_end

# snippet_start ext_current
# ----- External current options ------------------------

# Switch on external current density
ExternalCurrent = yes

# External current parameters
# - Column 1: input option for the td function which is here a predefined function
# - Column 2: spatial distribution of the current in x-direction
# - Column 3: spatial distribution of the current in y-direction
# - Column 4: spatial distribution of the current in z-direction
# - Column 5: frequency of the temporal current density pulse
# - Column 6: name of the temporal current pulse envelope function
%UserDefinedMaxwellExternalCurrent
current_td_function | "jx(x.y,z)" | "jy(x,y,z)" | "jz(x,y,z)" | omega | "env_func"
%

# Function in time
# - Column 1: name of the envelope function
# - Column 2: function type
# - Column 3: amplitude
# - Column 4: envelope function width
# - Column 5: shift of the function f(t-t0)
# t0 = time shift of the current density signal
# tw = width of the current density signal in time
%TDFunctions
"env_func” | tdf_gaussian | 1.0 | tw | t0
%
# snippet_end

# snippet_start inst_field
# ----- Spatial constant field propagation ------------------------------------------------------
# for this feature to work, boundaries must be set to constant
%MaxwellBoundaryConditions
constant | constant | constant
%

Ez           = 0.000010000000
By           = 0.000010000000
pulse_width  = 500.0
pulse_shift  = 270.0
pulse_slope  = 100.0

# Column 1, 2, 3: constant electric field component in x,y,z-direction
# Column 4, 5, 6: constant magnetic field component in x,y,z-direction
# Column 7: name of the td function

%UserDefinedConstantSpatialMaxwellField
0 | 0 | Ez | 0 | By | 0 | "time_function"
%

%TDFunctions
"time_function" | tdf_logistic | 1.0 | pulse_slope | pulse_width | pulse_shift
%
# snippet_end
