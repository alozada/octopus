# ----- Calculation mode and parallelization ------------------------------------------------------

 CalculationMode   = td
 RestartWrite = no
 ExperimentalFeatures = yes

%Systems
  'Maxwell' | maxwell
%

 Maxwell.ParDomains = auto
 Maxwell.ParStates  = no

# snippet_start box
# ----- Maxwell box variables ---------------------------------------------------------------------

 # free maxwell box limit of 10.0 plus 5.0 for absorbing boundary conditions

 lsize_mx = 15.0
 dx_mx    = 0.5

 Maxwell.BoxShape   = parallelepiped

 %Maxwell.Lsize
  lsize_mx | lsize_mx | lsize_mx
 %

 %Maxwell.Spacing
  dx_mx | dx_mx | dx_mx
 %
# snippet_end

# ----- Maxwell calculation variables -------------------------------------------------------------

 MaxwellHamiltonianOperator = faraday_ampere

# snippet_start boundaries
 %MaxwellBoundaryConditions
  zero | zero | zero
 %

 %MaxwellAbsorbingBoundaries
  cpml | cpml | cpml
 %

 MaxwellABWidth = 5.0
 MaxwellABPMLPower              = 2.0
 MaxwellABPMLReflectionError    = 1e-16
# snippet_end

# ----- Output variables --------------------------------------------------------------------------

 OutputFormat = axis_x + axis_y + axis_z + plane_x + plane_y + plane_z


# ----- Maxwell output variables ------------------------------------------------------------------

 %MaxwellOutput 
  electric_field 
  magnetic_field 
  maxwell_energy_density 
  trans_electric_field
 % 

 MaxwellOutputInterval = 10
 MaxwellTDOutput       = maxwell_energy + maxwell_total_e_field

 %MaxwellFieldsCoordinate
   0.00 | 0.00 | 0.00
 %


# ----- Time step variables -----------------------------------------------------------------------

 TDSystemPropagator = exp_mid
 timestep             = 1 / ( sqrt(c^2/dx_mx^2 + c^2/dx_mx^2 + c^2/dx_mx^2) )
 TDTimeStep           = timestep 
 TDPropagationTime    = 180 * timestep

# Maxwell field variables

# snippet_start current
# ----- External current ------------------------------------------------------------------------

 ExternalCurrent = yes
 t1 = 4 * 5.0 / c
 t2 = 6 * 5.0 / c
 tw = 0.03
 j = 1.0000

 %UserDefinedMaxwellExternalCurrent
   current_td_function | "0" | "0" | "j*exp(-x^2/2)*exp(-y^2/2)*exp(-z^2/2)" | 0 | "env_func_1"
   current_td_function | "0" |" 0" | "j*exp(-x^2/2)*exp(-y^2/2)*exp(-z^2/2)" | 0 | "env_func_2"
 %

 %TDFunctions
   "env_func_1" | tdf_gaussian |  1.0 | tw | t1
   "env_func_2" | tdf_gaussian | -1.0 | tw | t2
 %
# snippet_end